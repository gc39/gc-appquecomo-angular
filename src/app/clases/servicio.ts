export class Servicio {

    id:string;
    servicio:string;
    tipo:string;
    nombre:string;
    calorias:string;
    fecha:string;
    created:string;
    modified:string;
    plato:string;
}
