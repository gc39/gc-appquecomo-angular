import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { Servicio } from '../clases/servicio';
import { ServiceService } from '../services/service.service';

@Pipe({
  name: 'pipemenus'
})
export class PipemenusPipe implements PipeTransform {
constructor(private servicio:ServiceService){

}
  transform(arreglo: Servicio[], texto:string): Servicio[] {
     let  servicio:Servicio[];
     const fecha = Date.now();
     servicio     = arreglo.filter((x)=>x.tipo === texto );
     servicio     = servicio.filter((filtrar) =>  filtrar.fecha   === moment(fecha).format( "DD-MM-YYYY"));
     console.log(" esto es lo que devuelvo arreglo :  " + JSON.stringify(servicio ));
     if (servicio === []) {
      this.servicio.evaluaNoHayContenidoPipe = false;
     }
    return servicio;
  }
}
