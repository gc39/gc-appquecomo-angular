import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Branch } from '../clases/branch';
import { Servicio } from '../clases/servicio';
import { filter, find, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  branch: Branch;
  servicio: Servicio[];
  datosBranch:Observable<Branch[]> ;
  evaluaNoHayContenidoPipe:boolean = true;
  constructor(private http: HttpClient,
    private router: Router) {}

  getQuery(casino: string): Observable<any> {
      return this.http.get('https://quecomo.digitalboard.app/prod/backend/get_branch_info.php?branch_id=' + casino); ;
    }
  getDatosMenu(): Observable<any> {
    this.branch = JSON.parse(localStorage.getItem('URL'));

    return this.http.get(this.branch.menu);
  }

  getSugerenciaDelChef():Observable<any>{
    this.branch = JSON.parse(localStorage.getItem('URL'));

    return this.http.get(this.branch.sugerencia);
  }

  validarLocalStorage() {
    
   this.branch = JSON.parse(localStorage.getItem("URL"));

   if (!this.branch) {
    
    this.router.navigate(['/tabs/tab1']);
   }
  }

  validaQueHayaSession():boolean{
    this.branch = JSON.parse(localStorage.getItem('URL'));
    if ( this.branch === undefined) {
      return false
    } else {
      return true;
    }
  }
}
