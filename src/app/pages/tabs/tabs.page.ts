import { Component } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private  servicio: ServiceService) {}

valdidaSession():boolean{

  if (this.servicio.validaQueHayaSession) {
    return true;
  }else{
    return false;
  }
}

}
