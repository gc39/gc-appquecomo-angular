import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-buscador',
  templateUrl: './modal-buscador.component.html',
  styleUrls: ['./modal-buscador.component.scss'],
})
export class ModalBuscadorComponent implements OnInit {


  casino:String = new String();
  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {}


  cancel(){
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm(){
    return this.modalCtrl.dismiss(this.casino, 'confirm');
  }
}
