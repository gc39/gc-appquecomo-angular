import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { ExploreContainerComponentModule } from 'src/app/explore-container/explore-container.module';
import { ServiceService } from 'src/app/services/service.service';
import { ModalBuscadorComponent } from './modal-buscador/modal-buscador.component';
import { TabsPageModule } from '../tabs/tabs.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab1PageRoutingModule,
    
  ],
  providers: [ ],



  declarations: [Tab1Page, ModalBuscadorComponent]
})
export class Tab1PageModule {}
