import { Component, Host } from '@angular/core';
import { Branch } from 'src/app/clases/branch';
import { ServiceService } from 'src/app/services/service.service';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { LoadingController, ModalController } from '@ionic/angular';
import { ModalBuscadorComponent } from './modal-buscador/modal-buscador.component';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  casino: string;
  branch: Branch = new Branch();
  otro = 'Cl191201';
  mensage: string;
  isModalOpen = false;
  veronover: boolean;
  verImagenDeEspera: boolean;

  constructor(
    private servicio: ServiceService,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.veronover = false;
    //this.llamarDatosDeConexion(this.otro);
  }
  llamarDatosDeConexion(buscar) {
    this.showLoading();
    this.servicio.getQuery(buscar).subscribe((datos) => {
      this.branch = datos;
      // console.log(this.branch);

      if (datos.error === 'NOT_FOUND') {
        console.log(this.branch);
        this.setOpen(true);
        this.veronover = true;
      } else {
        
        localStorage.setItem('URL', JSON.stringify(this.branch));
        this.servicio.branch = this.branch;
        this.servicio.getDatosMenu().subscribe((datos) => {
          
          this.servicio.servicio = datos 
            if(this.servicio.servicio  != undefined){

              console.log("esto es lo que trae this.servicio.servicio  :   "  + this.servicio.servicio );
              this.navigate();

            }
     
          
        });
      }
    });
  }

  _filterUsuario(valueUsuario: string) {
    var filterValueUsuario = valueUsuario;
    return this.servicio.getQuery(valueUsuario).pipe(
      map((name) =>
        name.filter((filtraUsuario) => {
          return filtraUsuario.name.toLowerCase().includes(filterValueUsuario);
        })
      )
    );
  }

  async cargarCasino() {
    const modal = await this.modalCtrl.create({
      component: ModalBuscadorComponent,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      this.mensage = `Hello, ${data}!`;

      console.log('esto es lo que viene en data:  ' + data);
      this.llamarDatosDeConexion(data);
    }
  }
  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
  }
  async showLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Estamos buscando su Casino...',
      duration: 3000,
    });

    loading.present();

  }
  navigate() {
    this.router.navigate(['/tabs/tab2']);
  }
}
