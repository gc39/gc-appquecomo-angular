import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonSegment, ModalController } from '@ionic/angular';
import { Branch } from 'src/app/clases/branch';
import { Imagenes } from 'src/app/clases/imagenes';
import { Servicio } from 'src/app/clases/servicio';
import { TipoServicio } from 'src/app/clases/tipo-servicio';
import { PipemenusPipe } from 'src/app/pipes/pipemenus.pipe';
import { ServiceService } from 'src/app/services/service.service';
import { ModalSugerenciaDelChefComponent } from './modal-sugerencia-del-chef/modal-sugerencia-del-chef.component';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.css'],
})
export class Tab2Page {
  branch: Branch = new Branch();
  serviciosBranchAlmuerzo: Servicio[] = [];
  serviciosBranchDesayuno: Servicio[] = [];
  serviciosBranchCenaTarde: Servicio[] = [];
  serviciosBranchOnce: Servicio[] = [];
  serviciosBranch: Servicio[] = [];
  menus: Servicio[] = [];
  tipoDeMenu: any[] = [];
  tipoServicio: TipoServicio = new TipoServicio();
  imagenDeFondo:string;
  tiposDeImagenes:Imagenes = new Imagenes();
  activo:string;
  @ViewChild(IonSegment)  mySegment: IonSegment;
  constructor(public  servicio: ServiceService,
    private modalCtrl: ModalController) { }
  ngAfterViewInit(): void {
  setTimeout(() => {
    this.mySegment.value ='Desayuno';
  }, 2000);
    
  } 
  message = 'This modal example uses the modalController to present and dismiss modals.';

  segmentChanged(ev: any) {
    if (ev.detail.value === this.tipoServicio.Desayuno) {
      this.tipoDeMenu = this.filtrarDatosPorTipoMenu(this.serviciosBranchDesayuno);
      this.menus = this.serviciosBranchDesayuno;
      this.imagenDeFondo = this.tiposDeImagenes.Desayuno;
     // console.log("Desayuno  tipoDeMenu  : "+JSON.stringify(this.serviciosBranchDesayuno));
     // console.log("Desayuno menus  : "+JSON.stringify(this.menus));
     // console.log("Desayuno imagenDeFondo  : "+JSON.stringify(this.imagenDeFondo));
    }
    if (ev.detail.value === this.tipoServicio.Almuerzo) {
      this.tipoDeMenu = this.filtrarDatosPorTipoMenu(this.serviciosBranchAlmuerzo);
      this.menus = this.serviciosBranchAlmuerzo;
      this.imagenDeFondo = this.tiposDeImagenes.Almuerzo;
    }
    if (ev.detail.value === this.tipoServicio.Cena_Tarde) {
      this.tipoDeMenu = this.filtrarDatosPorTipoMenu(this.serviciosBranchCenaTarde);
      this.menus = this.serviciosBranchCenaTarde;
      this.imagenDeFondo = this.tiposDeImagenes.Cena_Tarde;
    }
    if (ev.detail.value === this.tipoServicio.once) {
      this.tipoDeMenu = this.filtrarDatosPorTipoMenu(this.serviciosBranchOnce);
      this.menus = this.serviciosBranchOnce;
      this.imagenDeFondo = this.tiposDeImagenes.once;
    }
  }
  ngOnInit(): void {
    this.servicio.validarLocalStorage();
   this.traerServicio();
  
  }

  traerServicio() {
    this.serviciosBranch = this.servicio.servicio;
   // console.log("traigo servicios ")
        
    //  console.log("Esto es lo que trae servicio  : "+JSON.stringify(this.serviciosBranch));

      this.serviciosBranchDesayuno = this.serviciosBranch.filter(
        (x) => x.servicio === 'Desayuno'
      );
      
      this.serviciosBranchAlmuerzo = this.serviciosBranch.filter(
        (x) => x.servicio === 'Almuerzo'
      );
      
      this.serviciosBranchCenaTarde = this.serviciosBranch.filter(
        (x) => x.servicio === 'Cena Tarde'
      );
      this.serviciosBranchOnce = this.serviciosBranch.filter(
        (x) => x.servicio === 'once');
         
      this.branch = this.servicio.branch;
      console.log("Desayuno  : "+JSON.stringify(this.serviciosBranchDesayuno));
      console.log("Almuerzo  : "+JSON.stringify(this.serviciosBranchAlmuerzo));
      console.log("Cena Tarde  : "+JSON.stringify(this.serviciosBranchCenaTarde));
      console.log("once  : "+JSON.stringify(this.serviciosBranchOnce));
  }

  filtrarDatosPorTipoMenu(dato: Array<any>) {
    var nuevo = dato.map((x) => x.tipo);
    nuevo = nuevo.filter((item, index) => {
      return nuevo.indexOf(item) === index;
    });
    return nuevo;
  }

  async  verSugerenciaDelChef(){
    const modal = await this.modalCtrl.create({
      component: ModalSugerenciaDelChefComponent,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      this.message = `Hello, ${data}!`;
    }
  }
}
