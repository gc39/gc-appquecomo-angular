import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Imagenes } from 'src/app/clases/imagenes';
import { Servicio } from 'src/app/clases/servicio';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-modal-sugerencia-del-chef',
  templateUrl: './modal-sugerencia-del-chef.component.html',
  styleUrls: ['./modal-sugerencia-del-chef.component.scss'],
})
export class ModalSugerenciaDelChefComponent implements OnInit {
  sericioMenu:Servicio[];
  name: string;
  tiposDeImagenes:Imagenes = new Imagenes();
ngOnInit(): void {
  this.cargarSugerenciaDelChef();
  
}
  constructor(private modalCtrl: ModalController, private servicio:ServiceService) {}

  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(this.name, 'confirm');
  }

  cargarSugerenciaDelChef(){

    this.servicio.getSugerenciaDelChef().subscribe(datos=> {
      console.log(datos)
      this.sericioMenu = datos;
    }) ;
  }
}
