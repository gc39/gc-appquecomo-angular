import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';


import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ExploreContainerComponentModule } from 'src/app/explore-container/explore-container.module';
import { PipemenusPipe } from 'src/app/pipes/pipemenus.pipe';
import { ServiceService } from 'src/app/services/service.service';
import { TabsPageModule } from '../tabs/tabs.module';
import { ModalSugerenciaDelChefComponent } from './modal-sugerencia-del-chef/modal-sugerencia-del-chef.component';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab2PageRoutingModule,
  
  ],
  declarations: [Tab2Page,PipemenusPipe, ModalSugerenciaDelChefComponent],
  providers: []
})
export class Tab2PageModule {}
