import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Que_como',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
