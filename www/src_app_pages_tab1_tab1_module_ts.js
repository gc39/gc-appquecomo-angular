"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_tab1_tab1_module_ts"],{

/***/ 4410:
/*!***********************************************************************!*\
  !*** ./src/app/pages/tab1/modal-buscador/modal-buscador.component.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModalBuscadorComponent": () => (/* binding */ ModalBuscadorComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _modal_buscador_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modal-buscador.component.html?ngResource */ 2872);
/* harmony import */ var _modal_buscador_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal-buscador.component.scss?ngResource */ 9227);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 3819);





let ModalBuscadorComponent = class ModalBuscadorComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.casino = new String();
    }
    ngOnInit() { }
    cancel() {
        return this.modalCtrl.dismiss(null, 'cancel');
    }
    confirm() {
        return this.modalCtrl.dismiss(this.casino, 'confirm');
    }
};
ModalBuscadorComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ModalController }
];
ModalBuscadorComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-modal-buscador',
        template: _modal_buscador_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_modal_buscador_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ModalBuscadorComponent);



/***/ }),

/***/ 8203:
/*!***************************************************!*\
  !*** ./src/app/pages/tab1/tab1-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageRoutingModule": () => (/* binding */ Tab1PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 4029);




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page,
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ 6789:
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageModule": () => (/* binding */ Tab1PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 4029);
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab1-routing.module */ 8203);
/* harmony import */ var src_app_explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/explore-container/explore-container.module */ 581);
/* harmony import */ var _modal_buscador_modal_buscador_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-buscador/modal-buscador.component */ 4410);









let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__.FormsModule,
            src_app_explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_2__.ExploreContainerComponentModule,
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_1__.Tab1PageRoutingModule,
        ],
        providers: [],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page, _modal_buscador_modal_buscador_component__WEBPACK_IMPORTED_MODULE_3__.ModalBuscadorComponent]
    })
], Tab1PageModule);



/***/ }),

/***/ 4029:
/*!*****************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1Page": () => (/* binding */ Tab1Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page.html?ngResource */ 7374);
/* harmony import */ var _tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab1.page.scss?ngResource */ 3398);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var src_app_clases_branch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/clases/branch */ 9985);
/* harmony import */ var src_app_services_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/service.service */ 58);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 6942);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _modal_buscador_modal_buscador_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modal-buscador/modal-buscador.component */ 4410);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 2816);










let Tab1Page = class Tab1Page {
    constructor(servicio, modalCtrl, loadingCtrl, router) {
        this.servicio = servicio;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.branch = new src_app_clases_branch__WEBPACK_IMPORTED_MODULE_2__.Branch();
        this.otro = 'Cl191201';
        this.isModalOpen = false;
    }
    ngOnInit() {
        this.veronover = false;
        //this.llamarDatosDeConexion(this.otro);
    }
    llamarDatosDeConexion(buscar) {
        this.showLoading();
        this.servicio.getQuery(buscar).subscribe((datos) => {
            this.branch = datos;
            // console.log(this.branch);
            if (datos.error === 'NOT_FOUND') {
                console.log(this.branch);
                this.setOpen(true);
                this.veronover = true;
            }
            else {
                localStorage.setItem('URL', JSON.stringify(this.branch));
                this.servicio.branch = this.branch;
                this.servicio.getDatosMenu().subscribe((datos) => {
                    this.servicio.servicio = datos;
                    if (this.servicio.servicio != undefined) {
                        console.log("esto es lo que trae this.servicio.servicio  :   " + this.servicio.servicio);
                        this.navigate();
                    }
                });
            }
        });
    }
    _filterUsuario(valueUsuario) {
        var filterValueUsuario = valueUsuario;
        return this.servicio.getQuery(valueUsuario).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((name) => name.filter((filtraUsuario) => {
            return filtraUsuario.name.toLowerCase().includes(filterValueUsuario);
        })));
    }
    cargarCasino() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_buscador_modal_buscador_component__WEBPACK_IMPORTED_MODULE_4__.ModalBuscadorComponent,
            });
            modal.present();
            const { data, role } = yield modal.onWillDismiss();
            if (role === 'confirm') {
                this.mensage = `Hello, ${data}!`;
                console.log('esto es lo que viene en data:  ' + data);
                this.llamarDatosDeConexion(data);
            }
        });
    }
    setOpen(isOpen) {
        this.isModalOpen = isOpen;
    }
    showLoading() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Estamos buscando su Casino...',
                duration: 3000,
            });
            loading.present();
        });
    }
    navigate() {
        this.router.navigate(['/tabs/tab2']);
    }
};
Tab1Page.ctorParameters = () => [
    { type: src_app_services_service_service__WEBPACK_IMPORTED_MODULE_3__.ServiceService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router }
];
Tab1Page = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-tab1',
        template: _tab1_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tab1_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], Tab1Page);



/***/ }),

/***/ 9227:
/*!************************************************************************************!*\
  !*** ./src/app/pages/tab1/modal-buscador/modal-buscador.component.scss?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtb2RhbC1idXNjYWRvci5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 3398:
/*!******************************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.scss?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = ".content {\n  padding-top: 40%;\n  background-color: #F76634;\n}\n\n.labelInput {\n  display: block;\n  padding: 3%;\n}\n\n.margenes {\n  margin: 10%;\n  text-align: center;\n}\n\n.contendorCard {\n  margin-top: 40%;\n  text-align: center;\n}\n\n.contendor {\n  margin-top: 40%;\n  text-align: center;\n  background-color: #F76634;\n}\n\n.labelInput {\n  text-align: center;\n}\n\n.imgsodexo {\n  height: 100%;\n  width: 40%;\n}\n\n.Colimgsodexo {\n  text-align: center;\n}\n\n.formatodeFondo {\n  background-color: #F76634;\n}\n\n.titulo {\n  text-align: center;\n  background-color: #F76634;\n  font-family: \"Lucida Sans\", \"Lucida Sans Regular\", \"Lucida Grande\", \"Lucida Sans Unicode\", Geneva, Verdana, sans-serif;\n  color: #2C2F8B;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYjEucGFnZS5zY3NzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vRnVlbnRlcyUyMEdDJTIwLSUyMDIwMjIvQVBQLVF1ZSVDQyU4MUNvJUNDJTgxbW8tQW5ndWxhci9RdWVfY29tby9zcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFRyxnQkFBQTtFQUNBLHlCQUFBO0FDQUg7O0FESUE7RUFDSSxjQUFBO0VBQ0EsV0FBQTtBQ0RKOztBREdBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDQUE7O0FER0E7RUFFSSxlQUFBO0VBQ0ksa0JBQUE7QUNEUjs7QURJQTtFQUVJLGVBQUE7RUFDSSxrQkFBQTtFQUNBLHlCQUFBO0FDRlI7O0FESUE7RUFDSSxrQkFBQTtBQ0RKOztBREdBO0VBRUksWUFBQTtFQUNBLFVBQUE7QUNESjs7QURHQTtFQUVJLGtCQUFBO0FDREo7O0FESUE7RUFDSSx5QkFBQTtBQ0RKOztBRElDO0VBQ0ksa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHNIQUFBO0VBQ0EsY0FBQTtBQ0RMIiwiZmlsZSI6InRhYjEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRlbnR7XG5cbiAgIHBhZGRpbmctdG9wOiA0MCUgO1xuICAgYmFja2dyb3VuZC1jb2xvcjogI0Y3NjYzNFxuXG59XG5cbi5sYWJlbElucHV0e1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmc6IDMlO1xufVxuLm1hcmdlbmVze1xubWFyZ2luOiAxMCU7XG50ZXh0LWFsaWduOiBjZW50ZXI7XG5cbn1cbi5jb250ZW5kb3JDYXJke1xuICAgXG4gICAgbWFyZ2luLXRvcDogNDAlIDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyOyAgXG4gICAgICAgIFxufVxuLmNvbnRlbmRvcntcbiAgIFxuICAgIG1hcmdpbi10b3A6IDQwJSA7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjsgIFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjc2NjM0O1xufVxuLmxhYmVsSW5wdXR7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmltZ3NvZGV4b3tcblxuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogNDAlO1xufVxuLkNvbGltZ3NvZGV4b3tcblxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvcm1hdG9kZUZvbmRve1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNGNzY2MzQ7XG4gICAgXG4gfVxuIC50aXR1bG97XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogI0Y3NjYzNDtcbiAgICAgZm9udC1mYW1pbHk6ICdMdWNpZGEgU2FucycsICdMdWNpZGEgU2FucyBSZWd1bGFyJywgJ0x1Y2lkYSBHcmFuZGUnLCAnTHVjaWRhIFNhbnMgVW5pY29kZScsIEdlbmV2YSwgVmVyZGFuYSwgc2Fucy1zZXJpZjtcbiAgICAgY29sb3I6IzJDMkY4QjtcbiB9IiwiLmNvbnRlbnQge1xuICBwYWRkaW5nLXRvcDogNDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjc2NjM0O1xufVxuXG4ubGFiZWxJbnB1dCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwYWRkaW5nOiAzJTtcbn1cblxuLm1hcmdlbmVzIHtcbiAgbWFyZ2luOiAxMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvbnRlbmRvckNhcmQge1xuICBtYXJnaW4tdG9wOiA0MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvbnRlbmRvciB7XG4gIG1hcmdpbi10b3A6IDQwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjc2NjM0O1xufVxuXG4ubGFiZWxJbnB1dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmltZ3NvZGV4byB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDQwJTtcbn1cblxuLkNvbGltZ3NvZGV4byB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvcm1hdG9kZUZvbmRvIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y3NjYzNDtcbn1cblxuLnRpdHVsbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0Y3NjYzNDtcbiAgZm9udC1mYW1pbHk6IFwiTHVjaWRhIFNhbnNcIiwgXCJMdWNpZGEgU2FucyBSZWd1bGFyXCIsIFwiTHVjaWRhIEdyYW5kZVwiLCBcIkx1Y2lkYSBTYW5zIFVuaWNvZGVcIiwgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICBjb2xvcjogIzJDMkY4Qjtcbn0iXX0= */";

/***/ }),

/***/ 2872:
/*!************************************************************************************!*\
  !*** ./src/app/pages/tab1/modal-buscador/modal-buscador.component.html?ngResource ***!
  \************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button color=\"medium\" (click)=\"cancel()\">Cancelar</ion-button>\n    </ion-buttons>\n    <ion-title>Bienvenido</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"confirm()\">Confirmar</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-label position=\"stacked\">Ingresa tu casino ! </ion-label>\n    <ion-input [(ngModel)]=\"casino\" placeholder=\"Escribe aqui tu casino !\"></ion-input>\n  </ion-item>\n</ion-content>";

/***/ }),

/***/ 7374:
/*!******************************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.html?ngResource ***!
  \******************************************************/
/***/ ((module) => {

module.exports = "<ion-header [translucent]=\"true\" >\n  <ion-toolbar>\n    <ion-title>\n      Cambiar Casino\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\" class=\"contenedor\" >\n\n  <ion-card class=\"contendorCard\">\n\n    <ion-card-header class=\"margenes\">\n      <ion-card-subtitle>Bienvenido</ion-card-subtitle>\n      <ion-card-title class=\"margenes\"></ion-card-title>\n      <img src=\"../../../assets/img/icono.png\" class=\"imgsodexo\" alt=\"\">\n    </ion-card-header>\n  \n    <ion-card-content >\n      <ion-button expand=\"block\" (click)=\"cargarCasino()\">\n        Buscar\n      </ion-button>\n    </ion-card-content>\n  </ion-card>\n\n\n  <ion-content class=\"ion-padding formatodeFondo\" *ngIf=\"veronover\" >\n    <ion-modal [isOpen]=\"isModalOpen\">\n      <ng-template>\n        <ion-header>\n          <ion-toolbar>\n            <ion-title>Ops! Algo anda mal!</ion-title>\n            <ion-buttons slot=\"end\">\n              <ion-button (click)=\"setOpen(false)\">Close</ion-button>\n            </ion-buttons>\n          </ion-toolbar>\n        </ion-header>\n        <ion-content class=\"ion-padding\">\n          <p style=\"text-align: center;\">\n           Casino no ha sido encontrado !\n           <br>\n           Revise el casino ingresado.\n  \n          </p>\n        </ion-content>\n      </ng-template>\n    </ion-modal>\n  </ion-content>\n\n</ion-content>\n\n";

/***/ })

}]);
//# sourceMappingURL=src_app_pages_tab1_tab1_module_ts.js.map